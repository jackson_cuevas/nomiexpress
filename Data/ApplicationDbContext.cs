﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using nomiexpress.Models;

namespace nomiexpress.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
            public DbSet<Empleado> Empleados { get; set; }

            public DbSet<Departamento> Departamentos { get; set; }

            public DbSet<Empresa> Empresas { get; set; }

            public DbSet<Pago> Pagos { get; set; }

             public DbSet<AsignarPago> AsignarPagos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Empleado>().ToTable("Empleado");
            modelBuilder.Entity<Departamento>().ToTable("Departamento");
            modelBuilder.Entity<Empresa>().ToTable("Empresa");
            modelBuilder.Entity<Pago>().ToTable("Pago");
            modelBuilder.Entity<AsignarPago>().ToTable("AsignarPago");

            modelBuilder.Entity<AsignarPago>()
                .HasKey(c => new {c.pagoID,c.empleadoID});

        }
    }
}
