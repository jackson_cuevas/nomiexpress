using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using nomiexpress.Models;


namespace nomiexpress.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            //context.Database.EnsureCreated();

            // Look for any students.
            if (context.Empleados.Any())
            {
                return;   // DB has been seeded
            }

             var pagos = new Pago[] {
                new Pago {ID = 1, Fecha = DateTime.Parse("2005-09-01"), Concepto = "Pago de quincena"},
                new Pago {ID = 2, Fecha = DateTime.Parse("2005-09-01"), Concepto = "Pago de quincena"},
                new Pago {ID = 3, Fecha = DateTime.Parse("2005-09-01"), Concepto = "Pago de quincena"},
            };

            foreach (Pago p in pagos)
            {
                context.Pagos.Add(p);
            }
            context.SaveChanges();

            var empleados = new Empleado[]
            {
                new Empleado { Nombre = "Carson",   Apellido = "Alexander", Cedula = "1231231222321", Telefono = "809-100-0000",
                    Cuenta = "345654", Posicion = "Developer",Salario = 12345678,EstadoCivil = "Soltero",Direccion = "En Algun Lugar",Email = "example@hotmail.com", Hiredate = DateTime.Parse("2010-09-01") },
                new Empleado { Nombre = "Meredith", Apellido = "Alonso", Cedula = "1231231222321", Telefono = "809-200-0000",
                   Cuenta = "345654", Posicion = "Developer", Salario = 12345678,EstadoCivil = "Soltero",Direccion = "En Algun Lugar",Email = "example@hotmail.com",Hiredate = DateTime.Parse("2012-09-01") },
                new Empleado { Nombre = "Arturo",   Apellido = "Anand",Cedula = "1231231222321", Telefono = "809-300-0000",
                   Cuenta = "345654", Posicion = "Developer",Salario = 12345678, EstadoCivil = "Soltero",Direccion = "En Algun Lugar",Email = "example@hotmail.com",Hiredate = DateTime.Parse("2013-09-01") },
                new Empleado { Nombre = "Gytis",    Apellido = "Barzdukas",Cedula = "1231231222321", Telefono = "809-400-0000",
                   Cuenta = "345654", Posicion = "Developer",Salario = 12345678, EstadoCivil = "Soltero",Direccion = "En Algun Lugar",Email = "example@hotmail.com",Hiredate = DateTime.Parse("2012-09-01") },
                new Empleado { Nombre = "Yan",      Apellido = "Li",Cedula = "1231231222321", Telefono = "809-000-5000",
                   Cuenta = "345654",Posicion = "Developer", Salario = 12345678,EstadoCivil = "Soltero",Direccion = "En Algun Lugar",Email = "example@hotmail.com",Hiredate = DateTime.Parse("2012-09-01") },
                new Empleado { Nombre = "Peggy",    Apellido = "Justice",Cedula = "1231231222321", Telefono = "809-600-0000",
                   Cuenta = "345654",Posicion = "Developer",Salario = 12345678,EstadoCivil = "Soltero", Direccion = "En Algun Lugar",Email = "example@hotmail.com",Hiredate = DateTime.Parse("2011-09-01") },
                new Empleado { Nombre = "Laura",    Apellido = "Norman",Cedula = "1231231222321", Telefono = "809-700-0000",
                    Cuenta = "345654",Posicion = "Developer",Salario = 12345678,EstadoCivil = "Soltero",Direccion = "En Algun Lugar",Email = "example@hotmail.com",Hiredate = DateTime.Parse("2013-09-01") },
                new Empleado { Nombre = "Nino",     Apellido = "Olivetto",Cedula = "1231231222321", Telefono = "809-800-0000",
                   Cuenta = "345654",Posicion = "Developer",Salario = 12345678,EstadoCivil = "Soltero",Direccion = "En Algun Lugar",Email = "example@hotmail.com",Hiredate = DateTime.Parse("2005-09-01") }
            };

            foreach (Empleado e in empleados)
            {
                context.Empleados.Add(e);
            }
            context.SaveChanges();

            
            var asignarPagos = new AsignarPago[] {
                new AsignarPago { pagoID = pagos.Single(c => c.Concepto == "Pago de quincena").ID,
                                  empleadoID = empleados.Single(c => c.Nombre == "Jackson").ID},
                new AsignarPago { pagoID = pagos.Single(c => c.Concepto == "Pago de quincena").ID,
                                  empleadoID = empleados.Single(c => c.Nombre == "Carson").ID},                  
            };

            foreach (AsignarPago ap in asignarPagos)
            {
                context.AsignarPagos.Add(ap);
            }
            context.SaveChanges();

        


        }
    }
}