﻿using Microsoft.AspNetCore.Mvc;
using nomiexpress.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nomiexpress.ViewComponents
{
    public class EmpleadosViewComponent : ViewComponent
    {
        public IRepositoryEmpleado RepositoryEmpleado { get; }

        public EmpleadosViewComponent(IRepositoryEmpleado repositorioEmpleados)
        {
            RepositoryEmpleado = repositorioEmpleados;

        }

        public IViewComponentResult Invoke()
        {
            var empleados = RepositoryEmpleado.ObtenerTodos();
            return View(empleados);
        }
    }
}
 