﻿using nomiexpress.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nomiexpress.Services
{
   public interface IRepositoryEmpleado
    {
        List<Empleado> ObtenerTodos();
    }
}
