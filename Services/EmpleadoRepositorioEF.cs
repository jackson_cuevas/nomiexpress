﻿using Microsoft.EntityFrameworkCore;
using nomiexpress.Data;
using nomiexpress.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nomiexpress.Services
{
    public class EmpleadoRepositorioEF : IRepositoryEmpleado
    {
        public ApplicationDbContext DbContext { get; }

        public EmpleadoRepositorioEF(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public List<Empleado> ObtenerTodos()
        {
            return DbContext.Empleados.ToList();
        }
    }
}
