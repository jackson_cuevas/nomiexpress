﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace nomiexpress.Models
{
    public class Departamento
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Nombre: ")]
        public string Nombre { get; set; }

        public int? EmpleadoID { get; set; }
        public Empleado Administrador { get; set; }

    }
}
