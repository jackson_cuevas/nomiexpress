namespace nomiexpress.Models
{
    public class AsignarPago
    {
        public int empleadoID { get; set; }
        public int pagoID { get; set; }

        public Empleado Empleados {get; set;}
        public Pago Pagos {get; set;}
        
    }
}