﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nomiexpress.Models
{
    public class Nomina
    {
        public int ID { get; set; }
        public DateTime Fecha { get; set; }
    }
}
