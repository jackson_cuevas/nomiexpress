﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nomiexpress.Models
{
    public class EmpleadoViewModel
    {
        public int EmpleadoID { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public decimal Salario { get; set; }
        public string Posicion { get; set; }
        public Departamento Departamento { get; set; }
    }
}
