﻿using System;
using System.ComponentModel.DataAnnotations;


namespace nomiexpress.Models
{
    public class Empresa
    {
        [Key]
        public int EmpleadoID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Nombre: ")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Direccion: ")]
        public string Direccion { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Telefono: ")]
        public string Telefono { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Email: ")]
        public string Email { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "RNC: ")]
        public string RNC { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Capital: ")]
        public decimal Capital { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha de Inicio: ")]
        public DateTime Fundada { get; set; }


        public Empleado Empleado { get; set; }

    }
}
