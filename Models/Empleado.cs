﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace nomiexpress.Models
{
    public class Empleado
    {

        public Empleado()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Nombre: ")]
        public string Nombre { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Apellido: ")]
        public string Apellido { get; set; }
        [Required]
        [StringLength(13)]
        [Display(Name = "Cedula: ")]
        public string Cedula { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha de Inicio: ")]
        public DateTime Hiredate { get; set; }

        [Required]
        [StringLength(12)]
        [Display(Name = "Telefono: ")]
        public string Telefono { get; set; }


        [Required]
        [StringLength(12)]
        [Display(Name = "No. Cuenta: ")]
        public string Cuenta { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Posicion: ")]
        public string Posicion { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Salario: ")]
        public decimal Salario { get; set; }

        [Required]
        [StringLength(12)]
        [Display(Name = "Estado Civil: ")]
        public string EstadoCivil { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Dirección: ")]
        public string Direccion { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Email: ")]
        public string Email { get; set; }

        [Display(Name = "Foto:")]
        public string FotoLink { get; set; }


        [Display(Name = "Nombre: ")]
        public string FullName
        {
            get
            {
                return Nombre + ", " + Apellido;

            }

        }
        public ICollection<AsignarPago> AsignarPagos { get; set; }
        public Departamento Departamento { get; set; }
    }
}
