using System;
using System.ComponentModel.DataAnnotations;

namespace nomiexpress.Models
{
    public class Pago
    {
        public int ID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }
        public string Concepto { get; set; }

        public Empleado persona { get; set; }
    }
}